import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        count: 0
    },
    reducers: {
        increment: (state, actions)=>{
            if(actions.payload){
                state.count += actions.payload
            }
           
        },
        decrement: (state, actions)=>{
            state.count -=actions.payload
        },
        reset: (state)=>{
            state.count = 0
        },
        // todoAdded: (state, action) =>{
        //     state.push(action.payload)
        // }
    }
})

export const {increment, decrement, reset, todoAdded} = counterSlice.actions

export default counterSlice.reducer