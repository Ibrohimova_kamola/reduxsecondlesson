import { useState } from "react";
import "./App.css";
import { counterSlice, decrement, increment, todoAdded } from "./counter/counterSlice";
import { connect, useSelector } from "react-redux";

function App(props) {
  const { counter } = useSelector((state) => state);
  const { increment, decrement } = props;
  const [inputValue, setInputValue] = useState(0);
  const [value, setValue] = useState("")
  console.log(todoAdded);
  console.log(props);
  return (
    <>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(+e.target.value)}
      /> <br />
      <button onClick={()=>increment(inputValue)}>Increment</button>
      <button onClick={()=>decrement(inputValue)}>Decrement</button>
      <h1>{counter.count}</h1>
    </>
  );
}
// function App(props) {
//   const { counter,todoAdded } = useSelector((state) => state);
//   const [value, setValue] = useState([])
//   // console.log(todoAdded);
//   // console.log(props);
//   return (
//     <>
//       <input
//         type="text"
//         value={value}
//         onChange={(e) => setValue(+e.target.value)}
//       />
//       <button onClick={()=>todoAdded(value)}>Decrement</button>
//       <h1>{counter.count}</h1>
//     </>
//   );
// }
function mapStateToProps(state) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return {
    increment: (param) => dispatch(increment(param)),
    decrement: (param) => dispatch(decrement(param)),
  };
}
export default connect((state) => state, mapDispatchToProps)(App)
